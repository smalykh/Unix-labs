#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char **argv)
{
        pid_t p;
        int out;
	int pipefd[2];
	char buf;
	if(pipe(pipefd))
	{
		perror("pipe error!\n");
		return -1;
	}

	switch(p = fork())
	{
                case -1:
                        perror("Smth go wrong in fork..\n");
                        close(pipefd[0]);
                        close(pipefd[1]);
			break;
                case 0:
                        // Child proccess
			// close unused file descriptors
                        close(pipefd[0]);

                        // replace standard output. Write in pipe
                        dup2(pipefd[1], STDOUT_FILENO);

			// Run another program
                        execvp(argv[1], &argv[1]);
                        break;
                default:
                        //Parent process
                        close(pipefd[1]);
		
			// Open output file	
   			out = open("out", O_WRONLY | O_TRUNC | O_CREAT, S_IRUSR | S_IRGRP | S_IWGRP | S_IWUSR);
			while(read(pipefd[0], &buf, 1) > 0)
			{
				//duplicate output in stdout and out-file
				write(out, &buf,1);			
				write(STDOUT_FILENO, &buf,1);
			}
			close(pipefd[0]);
			close(out);
			return 0;
        }
}
