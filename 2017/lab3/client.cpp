#include <sys/types.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <string.h> /* For strerror(3c) */
#include <errno.h> /* For errno */
#include <unistd.h> /* rand(3c) */
#include <stdio.h>
#include <stdlib.h>
#include "server.h"

enum client_command {CLOSE_CMD, OPEN_CMD, IDLE};
int main (int argc, char **argv)
{
	if(argc < 2) 
	{
		printf("Specify index of client..\n");
		return -1;
	}
        int src_index = atoi(argv[1]);
	
	key_t ipckey;
        int semid;
        struct sembuf sem[3]; /* sembuf defined in sys/sem.h */
 
        /* Generate the ipc key */
        ipckey = ftok("/tmp/file", 42);
 	printf("IPC KEY is %d\n", ipckey);
        /* Set up the semaphore set. 4 == READ, 2 == ALTER */
        semid = semget(ipckey, MAX_CLIENTS + 1, 0666 | IPC_CREAT);
        
	if (semid < 0) {
                printf("Error - %s %d\n", strerror(errno), errno);
                _exit(1);
        }
 	for(int i=0; i < 2; i++)
	{
       	 	sem[i].sem_flg 	= SEM_UNDO; /* Release semaphore on exit */
	}
       
	printf("Client started..\n");
	printf("Client index is %d\n", src_index);
         
	while(1) 
	{	
		char command	[16];
		int lines	= 0;
		int dst_index 	= 0;

		scanf("%s %d %d", &command, &dst_index, &lines);
 		
		client_command cmd = IDLE;
		if(strcmp(command, "open" ) == 0) cmd = OPEN_CMD;
 		if(strcmp(command, "close") == 0) cmd = CLOSE_CMD;

		switch(cmd)
		{
			case OPEN_CMD:
				printf("Try to open connection with %d line width %d.\n", dst_index, lines);
 				sem[0].sem_op 	= -lines;
				sem[0].sem_num 	=  0;
                		sem[1].sem_op 	= -lines;
				sem[1].sem_num 	=  dst_index;
                		sem[2].sem_op 	= -lines;
				sem[2].sem_num 	=  src_index;
                		semop(semid, sem, 3);
				printf("Open connection with %d line width %d.\n", dst_index, lines);
			break;	
			case CLOSE_CMD:
				sem[0].sem_op 	=  lines;
				sem[0].sem_num 	=  0;
                		sem[1].sem_op 	=  lines;
				sem[1].sem_num 	=  dst_index;
                		sem[2].sem_op 	=  lines;
				sem[2].sem_num 	=  src_index;
                		semop(semid, sem, 3);
			break;
			deafault:
				fprintf(stderr, "Should not happen..\n");
			break;
		}
 
        }
 
}
