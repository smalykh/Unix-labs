#include <sys/types.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <string.h> /* For strerror(3c) */
#include <errno.h> /* For errno */
#include <unistd.h> /* rand(3c) */
#include <stdio.h>
#include "server.h"
union semun  
    {
        int val;
        struct semid_ds *buf;
        ushort array [1];
    };

void init_semaphore( int sid, int semnum, int initval)
{
        union semun semopts;    

        semopts.val = initval;
        semctl( sid, semnum, SETVAL, semopts);
}
int main (int argc, char **argv) {
 
        key_t ipckey;
        int semid;
        struct sembuf sem[MAX_CLIENTS + 1]; /* sembuf defined in sys/sem.h */
 
        /* Generate the ipc key */
        ipckey = ftok("/tmp/file", 42);
 	printf("IPC KEY is %d\n", ipckey);
        /* Set up the semaphore set. 4 == READ, 2 == ALTER */
        semid = semget(ipckey, MAX_CLIENTS + 1, 0666 | IPC_CREAT);
        if (semid < 0) {
                printf("Error - %s %d\n", strerror(errno), errno);
                _exit(1);
        }
 
        /* These never change so leave them outside the loop */
	init_semaphore(semid, 0, SERVER_LINES);
        for(int i=1; i < SERVER_LINES; i++)
	{	
		init_semaphore(semid, i, CLIENT_LINES);
	}
//        semop(semid, sem, SERVER_LINES);
                
	printf("Server started..\n");
         
	while(1) 
	{
		int vals[MAX_CLIENTS + 1];
		for(int i=0; i < MAX_CLIENTS + 1; i++)
		{
			printf("Client %d\t%d\n", i, semctl(semid, i, GETVAL, 0)); 	 
        	}
		sleep(5);
		printf("\n");	
	}
 
}
