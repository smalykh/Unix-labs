#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#define MAX_DIM		(5)

int matrix_process(int dim, int P);

int main(int argc, char **argv)
{
	int i, j, n;
	if(argc < 3) {
		fprintf(stderr, "Usage: %s <dimension_of_matrix> <power>\n", argv[0]);
		return 1;
	}
	int dim = atoi(argv[1]);
	int P = atoi(argv[2]);
	
	printf("Current concurrency level: %d\n", pthread_getconcurrency());
	printf("Set new concurrency level: ");
	scanf("%d", &n);
	if(n != 0) pthread_setconcurrency(n);
	printf("New concurrency level: %d\n", pthread_getconcurrency());
	matrix_process(dim,  P);
}



