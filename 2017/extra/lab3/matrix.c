#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#define MAX_DIM		(5)

struct pos { int i; int j; int dim; };
static struct pos p[MAX_DIM][MAX_DIM];

static pthread_t tids[MAX_DIM][MAX_DIM];

static int matrix[MAX_DIM][MAX_DIM]; 	// Source matrix "as is"
static int imd_matrix[MAX_DIM][MAX_DIM]; 	// Intermediate matirix. 
					// Each of threads places its result here
static int result_matrix[MAX_DIM][MAX_DIM]; // Matrix after some mult operations

static void* mult(void *arg)
{
	struct pos *position = (struct pos*)arg;
	int ii = position->i;
	int jj = position->j;
	int dim = position->dim;
	
	for(int k = 0; k < dim; k++) 
		imd_matrix[ii][jj] += result_matrix[ii][k] * matrix[k][jj];
	return 0;
}

int show(int m[MAX_DIM][MAX_DIM], int dim)
{
	for(int i = 0; i < dim; i++) {
	 for(int j = 0; j < dim; j++)
		printf("%d ", m[i][j]);
	 printf("\n");
	}
}

int matrix_process(int dim, int P)
{
	int i, j;
	srand(1234);
	printf("Source matrix:\n");
	for(int i = 0; i < dim; i++) 
	 for(int j = 0; j < dim; j++) matrix[i][j] = rand() % 10;
	
	show(matrix, dim);		
	memcpy(result_matrix, matrix, sizeof(matrix));
	for(i = 0; i < dim; i++)
	 for(j = 0; j < dim; j++) {
		p[i][j].i = i;
		p[i][j].j = j;
		p[i][j].dim = dim;
	 }

	for(int power = 1 ; power < P; power++)
	{	
		for(i = 0; i < dim; i++)
		 for(j = 0; j < dim; j++)
			pthread_create(&tids[i][j], NULL, &mult, &p[i][j]);
		
		for(i = 0; i < dim; i++)
		 for(j = 0; j < dim; j++)
			pthread_join(tids[i][j], NULL);
	
		memcpy(result_matrix, imd_matrix, sizeof(matrix));
	}
	printf("Source matrix in %d power:\n", P);
	show(result_matrix, dim);
}
