#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>

#define MEMSIZE 	(16 * 1024)

void print_help(char *name)
{
	printf("Options:\n");
	printf(" create <id>         - create shm by id\n");
	printf(" check <id>          - check if shm exists by id\n");
	printf(" read <id>           - read string from shm\n");
	printf(" write <id> <string> - write string in shm\n");
}

int main(int argc, char **argv)
{
	while (1)
	{
		int i;
		char cmd[256];
		//for(i = 0; i < sizeof(cmd); i++) cmd[i] = 0;
 
		printf("Type action you need:\n(To print help type h, to quit type q)\n>");
		scanf("%s", cmd);

		if(strcmp(cmd, "create") == 0) {
			key_t key; int memid;
			int id; scanf("%d", &id); 

			key = ftok("./mem", id);
			if(key == -1) { perror("FTOK:"); continue; }

			memid = shmget(key, MEMSIZE, IPC_PRIVATE | IPC_CREAT | 0666);
			if(memid == -1) { perror("SHMGET:"); continue; }

			printf("Memory is created. id = %d\n", memid);		
		}
		else if(strcmp(cmd, "check") == 0) { 
			key_t key; int memid;
			int id; scanf("%d", &id); 

			key = ftok(argv[0], id);
			if(key == -1) { perror("FTOK:"); continue; }

			memid = shmget(key, MEMSIZE, 0666);
			if(memid == -1) printf ("Memory is not exists.\n");
			else printf ("Memory exists!\n");
		}
		else if(strcmp(cmd, "write") == 0) { 
			key_t key; int memid; void *shmem;
			int id; scanf("%d", &id); 

			key = ftok("./mem", id);
			if(key == -1) { perror("FTOK:"); continue; }

			memid = shmget(key, MEMSIZE, IPC_PRIVATE | 0666);
			if(memid == -1) { perror("SHMGET:"); continue; }
			
			shmem = shmat(memid, NULL, 0);
			if(shmem == -1) perror("SHMAT:\n");
			else scanf("%s", (char*)shmem);
			shmdt(shmem);
		}
		else if(strcmp(cmd, "read") == 0) { 
			key_t key; int memid; void *shmem;
			int id; scanf("%d", &id); 

			key = ftok("./mem", id);
			if(key == -1) { perror("FTOK:"); continue; }

			memid = shmget(key, MEMSIZE, IPC_PRIVATE | 0666);
			if(memid == -1) { perror("SHMGET:"); continue; }
			
			shmem = shmat(memid, NULL, 0);
			if(shmem == -1) perror("SHMAT:\n");
			else {
				((char*)shmem)[255] = 0; 
				printf("%s", (char*)shmem);
			}
			shmdt(shmem);
		}
		else if(strcmp(cmd, "q") == 0) break; 
		else print_help(argv[0]);
		printf("\n");	
	}
}
