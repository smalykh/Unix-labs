#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>

#define BUF_SIZE	1024

char buf[BUF_SIZE];

    		/* l_type   l_whence  l_start  l_len  l_pid   */
struct flock fl = { F_RDLCK, SEEK_SET, 0,       0,     0 };

int _lock(int fd, int start, int len)
{
    fl.l_start 	= start;
    fl.l_len 	= len;
    fl.l_pid 	= getpid();
    fl.l_type 	= F_RDLCK; 
    if (fcntl(fd, F_SETLKW, &fl) == -1) 
    {
    	perror("fcntl");
    	return  -1;
    }
    return 0;
}

int _unlock(int fd, int start, int len)
{
    fl.l_start 	= start;
    fl.l_len 	= len;
    fl.l_pid 	= getpid();
    fl.l_type 	= F_UNLCK;  /* set to unlock same region */
    
    if (fcntl(fd, F_SETLK, &fl) == -1) 
    {
    	perror("fcntl");
    	return -1;
    }
    return 0;
}

int main(int argc, char **argv)
{
    if(argc < 4)
    {
         fprintf(stderr, "Need 3 args\n");
         fprintf(stderr, "%s <filename> <position> <length>\n", argv[0]);
         return -1;
    }

    mode_t mode = 0777;//S_IRUSR | S_IWUSR;
    char *filename = argv[1];
    int length;
    int position;

    if(sscanf(argv[2], "%d", &position) != 1)
    {
         fprintf(stderr, "Error in 2 arg\n");
         return -1;
    }

    if(sscanf(argv[3], "%d", &length) != 1)
    {
         fprintf(stderr, "Error in 3 arg\n");
         return -1;
    }
    if(length <=0 || length >= BUF_SIZE -1)
    {
	fprintf(stderr, "length is out of range\n");
	return -1;
    }

    int fd = open(filename, O_CREAT| O_RDONLY, mode);
    if(fd < 0)
    {
        fprintf(stderr, "Error oppening file!\n");
    }

    // Lock file
    if(_lock(fd, position, length)) return -1;
 
    int n_pos = lseek(fd, position, SEEK_SET);
    if(n_pos != position)
    {
         fprintf(stderr, "Error in 2 arg\n");
         close(fd);
         return -1;
    }

    int len = read(fd, &buf, length);
    if(len < length) printf("Warning: only %d chars read\n", len);
    printf("String is %s\n", buf);
    sleep(10); 
   // Unclok file    
    if(_unlock(fd, position, length)) return -1;
    close(fd);
    return 0;
}
