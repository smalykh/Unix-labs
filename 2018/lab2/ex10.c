#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
extern char **environ;
int main(int argc, char **argv)
{
	pid_t pid;
    int status, i;

    char env[256*16];
    char cmd[256];
    
    char **penv = &environ[0];
    int offst = 0;

    // Output environment for each process
    while (*penv) 
        offst += sprintf((env+offst), "%s\n", *penv++);
    fprintf(stderr, "pid=%d:\n %s\n", getpid(), env);
    
    //Output argv for each process	
    for(i = (pid) ? 0 : 1, offst = 0; i < argc; i++)
    {   
          offst += sprintf(cmd+offst, "%s ", argv[i]);
    }
    fprintf(stderr, "argv for %s process: %s\n\n", (pid)? "parent" : "child", cmd);
   
	if((pid = fork()) == -1) {fprintf (stderr, "fork fail\n"); return 1;}
    if(pid) wait(&status);	
    else execvp (argv[1], &argv[1]);
}
