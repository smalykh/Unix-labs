#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
    int i, offst=0;
    char cmd[256] = {0};
    printf("=================\n");
    printf("system usage\n");
    printf("=================\n");
    for(i = 1; i < argc; i++)
    {
        offst += sprintf(cmd+offst, "%s ", argv[i]);
    }
    printf("%s\n", cmd);
    system(cmd);
    
    printf("\n\n=================\n");
    printf("fork exec usage\n");
    printf("=================\n");
    pid_t pid;
    
    switch(pid = fork())
    {
        case -1: 
                fprintf(stderr, "Oops..\n");
                return -1;
        case 0:
                execvp(argv[1], &argv[1]);
                break; // we need it?
        default:
            wait();
    }
}

