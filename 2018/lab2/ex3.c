#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main()
{
	pid_t pid;
	int status;
	fprintf(stderr, "Start main program..\n");
	pid = fork();
	fprintf(stderr, "pid=%d ppid=%d grp=%d\n", getpid(), getppid(), getpgrp());
	switch(pid)
	{
		case -1: 
			fprintf(stderr, "Error\n"); return -1;
		case 0: 
			return 1;
		default:
			printf("Wait while child process ends..\n");
			pid_t p =  wait(&status);
            printf("Done. st=%d pid=%d\n", WEXITSTATUS(status), p);  
			break;
	}
	
}
