#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define PARENT_FILE "parent.txt"
#define CHILD_FILE  "child.txt"

int main(int argc, char **argv)
{
	pid_t pid;
    int status, i;
    
    // Fork
	if((pid = fork()) == -1) { fprintf (stderr, "fork fail\n"); return 1; }
    
    // Open file for reading
    int fd = open(argv[1], O_RDONLY);
    if(fd < 0) { perror("cannot open file\n"); return 1; }

    // Open files for writing both in parent and in child
    char * filename = (pid)? PARENT_FILE : CHILD_FILE;
    int fd_out = open(filename, O_RDWR | O_CREAT, 0666);
   
    // Read symbol from input file and write it in output
    char ch;
    while (read(fd, &ch, 1)) 
            write(fd_out, &ch, 1);
    close(fd_out);
    
    if(pid) wait(&status); // Parent wait
    
    // Output to the screen
    fd_out = open(filename, O_RDONLY);
    while (read(fd, &ch, 1)) printf("%c", ch);
    close(fd_out);
    
}
