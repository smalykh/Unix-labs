#include <stdio.h>
static char *s;
int main(int argc, char **argv)
{
	if(argc < 3)
	{	
		fprintf(stderr, "Specfify var and value to add in env\n");
		return 1;
	}
	int ret = setenv(argv[1], argv[2], 1);
	printf("Retval=%d\n", ret);
	s = getenv(argv[1]);
	printf("Read from env..\n");
	printf("%s\n", s);
}
