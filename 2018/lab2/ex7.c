#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char **argv)
{
	pid_t pid;
	pid_t pgid;
	int status;
    int ret;


	fprintf(stderr, "Start main program..\n");
	pid = fork();
	fprintf(stderr, "pid=%d ppid=%d grp=%d\n", getpid(), getppid(), getpgrp());
	switch(pid)
	{
		case -1: 
			fprintf(stderr, "Error\n"); return -1;
		case 0: 
           // ret = setpgid(getpid(), pgid);
            ret = setpgrp();
            if(ret) perror("");
	        fprintf(stderr, "pid=%d ppid=%d grp=%d\n", getpid(), getppid(), getpgrp());
			pause();
			return 1;
		default:
			pause();
			break;
	}		
	
}
