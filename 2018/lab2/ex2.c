#include <stdio.h>
#include <unistd.h>

int main()
{
	pid_t pid;
	printf("Start main program..        ");
	switch(pid = fork())
	{
		case -1: _exit(1); 
			//fprintf(stderr, "Error\n"); return -1;
		case 0: _exit(0); 
			//fprintf(stderr, "Child start!\n"); break;
		default: _exit(0);
			//fprintf(stderr, "Parent start!\n"); wait();break;
	}
	
}
