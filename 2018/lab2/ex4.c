#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
main()
{
    extern int errno;
    pid_t pid;
    int rv;
    switch(pid=fork()) {
        case -1:
            perror("fork");
            exit(1);
        case 0:
            printf(" CHILD: This is child-process!\n");
            printf(" CHILD: My PID -- %d\n", getpid());
            printf(" CHILD: My parent PID -- %d\n", getppid());
            sleep(3);
            printf(" CHILD: My new parent PID -- %d\n", getppid());
            printf(" CHILD: Exit!\n");
            exit(rv);
        default:
	    sleep(1);
            printf("PARENT: This is parent-process!\n");
            printf("PARENT: My PID -- %d\n", getpid());
            printf("PARENT: My child PID %d\n",pid);
            printf("PARENT: Exit!\n");
    }
}
