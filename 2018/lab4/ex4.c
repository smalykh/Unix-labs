#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>


#define BLOCKING

int main(int argc, char ** argv) 
{
    int parent_pipe[2];
    int child_pipe[2];
    char buff[64];
    pid_t pid;
    int i;
    int in, out;
    char c = 0;

    if(argc == 1)
    {
	#ifdef BLOCKING
    	if(pipe(parent_pipe) || pipe(child_pipe))
	#else
   	if(pipe2(parent_pipe, O_NONBLOCK) | pipe2(child_pipe, O_NONBLOCK))
	#endif
   	{ 
	 	perror("pipe(...)");
      		exit(1);
	}

	switch(pid = fork())
	{
    		case -1:
        		perror("fork()");
        		exit(1);
		case 0:
			close(parent_pipe[0]);
			close(child_pipe[1]);

			dup2(parent_pipe[1], STDOUT_FILENO);
			dup2(child_pipe[0],  STDIN_FILENO);
	
			execlp(argv[0], argv[0], "child", NULL);
		default:
			close(parent_pipe[1]);
			close(child_pipe[0]);
			while (1) {
				if(read(STDIN_FILENO, &c, 1) > 0) {
       					write(child_pipe[1], &c, 1);
					read(parent_pipe[0], &c, 1);
					printf("%c", c);
				}
				else {
					fprintf(stderr, "Close pipe\n");
					close(parent_pipe[0]); 
					close(child_pipe[1]); 
					wait(NULL);
					exit(0);
				}
			}	
	}
    }
    else if (!strcmp(argv[1], "child"))
    {
	while(read(STDIN_FILENO, &c, 1) > 0) {
		write(STDOUT_FILENO, &c, 1);
	}
	exit(0);
    }
}
