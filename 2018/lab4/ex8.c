#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
char msg[] = "All work with no play make Jack a dull boy\n";
 
int main(int argc, char *argv[])
{
    	int fd;
   	pid_t pid; 
   	char *filename;
	if(argc < 2) return 1;
	
	filename = argv[1];
	 
	switch(pid = fork())
	{
		case -1:
			perror("");
			exit(1);
		case 0:
			fd = open(filename, O_RDONLY | O_CREAT, 0666);
			if(fd < 0) {perror(""); exit(1);}

			while(1)
			{
    				struct flock lock = { F_RDLCK, SEEK_SET, 0, 0, getpid()};
				char buf[128] = {0};
				if(fcntl(fd, F_SETLK, &lock) != -1) {
					int ret = read(fd, &buf, sizeof(msg));
					if(ret > 0) fprintf(stderr, "Readed: %s\n", buf);
				}
				else fprintf(stderr, ".");
				lock.l_type = F_UNLCK;
				fcntl(fd, F_SETLKW, &lock); 
			}
			fprintf(stderr, "Reader is ended\n");
			exit(0);
		default:
			fd = open(filename, O_WRONLY | O_CREAT, 0666); 
			if(fd < 0) {perror(""); exit(1);}

			while(1) {
    				struct flock lock = { F_WRLCK, SEEK_SET, 0, 0, getpid()};
				if(fcntl(fd, F_SETLKW, &lock) != -1) {
					if (write(fd, &msg, sizeof(msg)) <= 0) 
					break;
				}
				else fprintf(stderr, ".");
				lock.l_type = F_UNLCK;
				fcntl(fd, F_SETLKW, &lock); 
			}
	}
}
