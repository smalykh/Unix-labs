#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>


#define BLOCKING

int main(int argc, char ** argv) 
{
	int pipe_fd[2];
	pid_t pid;

	#ifdef BLOCKING
    	if(pipe(pipe_fd))
	#else
   	if(pipe2(pipe_fd, O_NONBLOCK))
	#endif
   	{ 
	 	perror("pipe(...)");
      		exit(1);
	}

	switch(pid = fork())
	{
    		case -1:
        		perror("fork()");
        		exit(1);
		case 0:
			close(pipe_fd[1]);

			dup2(pipe_fd[0],  STDIN_FILENO);
			execlp("wc", "wc", "-l", NULL);
		default:
			close(pipe_fd[0]);
			dup2(pipe_fd[1],  STDOUT_FILENO);
			execlp("who", "who", NULL);
	}
}
