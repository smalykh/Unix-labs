#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

char msg[] = "All work with no play make Jack a dull boy\n";

int main(int argc, char **argv)
{
	pid_t pid;
	int fd;
	pid = fork();	
	while(1) {
		char ch[8] = {0};
		if(read (0, &ch, 7) > 0)
			printf("%s:%s\n", (pid) ? "C" : "P", ch);
		else perror("");
	}
}
