#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>


#define BLOCKING

char child[]="Child Writes. Parent Reads\n";
char parent[]="Parent Writes. Child Reads\n";

int main() {
    int parent_pipe[2];
    int child_pipe[2];
    char buff[64];
    pid_t pid;
    int i;
    int in, out;
    char c = 0;
#ifdef BLOCKING
    if(pipe(parent_pipe))
#else
    if(pipe2(parent_pipe, O_NONBLOCK))
#endif
    { 
       perror("pipe(...)");
        exit(1);
    }

    switch(pid = fork())
    {
    	case -1:
        	perror("fork()");
        	exit(1);
	case 0:
		close(parent_pipe[0]);
		while(read(STDIN_FILENO, &c, 1) > 0)
			write(parent_pipe[1], &c, 1);
		fprintf(stderr, "Close pipe\n");
		exit(0);
    	default: 
		close(parent_pipe[1]);
		while(read(parent_pipe[0], &c, 1) > 0)
       			printf("%c", c);
		wait(NULL);
    }
}
