#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>

#define WRITE_SIZE 	50

#define BLOCKING
//#define DEAD

char child[]="Child Writes. Parent Reads\n";
char parent[]="Parent Writes. Child Reads\n";

void dead(int fd)
{
	char buf[2];
	#ifdef DEAD
      	if(read(fd, buf, 2)) fprintf(stderr, "errno=%d(%s)\n", errno, sys_errlist[errno]);
	sleep(3);
	#endif 
}
int main() {
    int parent_pipe[2];
    int child_pipe[2];
    char buff[64];
    pid_t pid;
    int i;
    int in, out;
#ifdef BLOCKING
    if(pipe(parent_pipe) || pipe(child_pipe)) {
#else
    if(pipe2(parent_pipe, O_NONBLOCK) || pipe2(child_pipe, O_NONBLOCK)) {
#endif 
       perror("pipe(...)");
        exit(1);
    }

    switch(pid = fork())
    {
    	case -1:
        	perror("fork()");
        	exit(1);
	case 0:
      	  	in  = child_pipe[0];
        	out = parent_pipe[1];
		dead(in);
		
	       	//for (i = 0; i < 10; ++i) {
		while(1) 
		{
            		write(out, child, strlen(child) + 1);
            		read(in,    buff, strlen(parent) + 1);
			fprintf(stderr, "Parent: %s", buff);
        	}
		exit(0);
    	default:
       		in  = parent_pipe[0];
       		out = child_pipe[1];
		dead(in);
		
		while(1) 
            	{
			read(in,     buff, strlen(child)  + 1);
			fprintf(stderr, "Child: %s", buff);
            		write(out, parent, strlen(parent) + 1);
        	}
		wait(NULL);
	}
}
