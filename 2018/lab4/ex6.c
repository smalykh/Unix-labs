#include <stdio.h>
#include <fcntl.h>
#include <string.h>

#define FIFO	"fifo"
#define MKFIFO
//#define DELAY

char msg[] = "All work with no play make Jack a dull boy\n";
int main(int argc, char **argv) 
{
	int fd, flags = 0;

	#ifdef DELAY
	flags = O_NDELAY;
	#endif

	#ifdef MKFIFO
	int rc = mknod(FIFO, S_IFIFO |  0666, 0);
	if(rc<0) perror("Error in mknod");
	#endif

	if(argc < 2) {fprintf(stderr, "Usage: %s [w | r] \n", argv[0]); return 1;}
	
	if(argv[1][0] == 'r')
	{
		fd = open(FIFO, O_RDONLY | flags);
		if(fd < 0) perror("--");
		while (1) {
			char info[50] = {0};	
			int n;
			n = read(fd, info, 50);
			if(n < 0) perror( "Read error");
 			if(n > 0) printf("Received message=%s\n",info);
		}
	}
	else if (argv[1][0] == 'w')
	{
		fd = open(FIFO, O_WRONLY | flags);
		if(fd < 0) perror("--");
		if(write(fd, msg, sizeof(msg)) < 0) perror("Write error");
	}
} 
