#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

char msg[] = "All work with no play make Jack a dull boy\n";

int main(int argc, char **argv)
{
	pid_t pid;
	int fd;
	char *filename;
	int cnt = 0;
	int readed;
	if(argc < 2) { 
		fprintf(stderr, "Usage: %s <filename>\n", argv[0]);
		return 1;
	}

	filename = argv[1];
	switch(pid = fork())
	{
		case -1:
			perror("");
			exit(1);
		case 0:
			fd = open(filename, O_RDONLY | O_CREAT, 0666);
			if(fd < 0) {perror(""); exit(1);}

			while(1)
			{
				char buf[128] = {0};
				int ret = read(fd, &buf, sizeof(msg));
				if(ret > 0) fprintf(stderr, "Readed: %s\n", buf);
			}
			fprintf(stderr, "Reader is ended\n");
			//kill(getppid(), SIGINT);
			exit(0);
		default:
			fd = open(filename, O_WRONLY | O_CREAT, 0666); 
			if(fd < 0) {perror(""); exit(1);}

			while(write(fd, &msg, sizeof(msg)) > 0);
	}
}
