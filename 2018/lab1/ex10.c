#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


int open_wrapper(char *filename, int flags, mode_t mode)
{
	int fd;
	if((fd = open(filename, flags, mode)) == -1)
	{
		perror("Error");
		fprintf(stderr, "Oops.. errno = %d, sys_errlist[%d] = %s\n",
					errno, errno, sys_errlist[errno]);
	}
	else printf("Open\n");
	return fd;
}

static char readbuf[256];
int main (int argc, char **argv)
{
	int fd, flags, n;
	off_t offset; 
	int whence;
	mode_t mode;
	
	if(argc < 2) 
	{
		fprintf(stderr, "Specify filename to recverse out\n");
		return -1;
	}
	flags = O_RDONLY;
	mode = 0666;
	
	printf("Open file(r)");
	fd = open_wrapper(argv[1], flags, mode);
	if(fd < 0) return -1;
	else printf("fd=%d\n", fd);
		
	char ch;
	off_t offt;
	for(offt = lseek(fd, 0, SEEK_END); offt >= 0; offt--)
	{
		read(fd, &ch, 1);
		printf("%c", ch);
		lseek(fd, offt, SEEK_SET);
	}
	printf("\n");
	close(fd);
	
	return 0;
}
