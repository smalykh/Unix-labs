#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

char message[] = "First string\r\nSecond string\r\nThird string\r\n";

int open_wrapper(char *filename, int flags, mode_t mode)
{
	int fd;
	if((fd = open(filename, flags, mode)) == -1)
	{
		perror("Error");
		fprintf(stderr, "Errno = %d, sys_errlist[%d] = %s\n",
					errno, errno, sys_errlist[errno]);
	}
	else printf("Open\n");
	return fd;
}

static char readbuf[256];
int main (int argc, char **argv)
{
	int fd, flags, n;
	mode_t mode;
	
	if(argc < 3) 
	{
		fprintf(stderr, "Specify filename to open and rights\n");
		return -1;
	}
	flags = O_CREAT | O_RDWR;
	if(sscanf(argv[2], "%o", &mode) != 1) return -1; 
	
	printf("Open file(rw + create)\n");
	fd = open_wrapper(argv[1], flags, mode);
	if(fd < 0) return -1;
	else printf("fd=%d\n", fd);

	printf("Try to write to file\n");
	n = write(fd, message, sizeof(message));
	printf("write retval=%d\n", n);
	printf("Close file\n");
	close(fd);

	printf("Open file (read only)\n");
	fd = open_wrapper(argv[1], O_RDONLY, 0);
	if(fd < 0) return -1;
	printf("Try to read from file\n");
	n = read(fd, readbuf, sizeof(message));
	
	printf("Filedata: %s\n", readbuf);
	printf("read retval=%d\n", n);
	printf("Close file\n");
	close(fd);

	printf("Open file (read/write)\n");
	fd = open_wrapper(argv[1], O_RDWR, 0);
	if(fd < 0) return -1;
	printf("Close file\n");
	close(fd);
	return 0;
}
