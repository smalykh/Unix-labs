#include <stdio.h>
#include <sys/types.h>
#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define BUFSIZE 1024

int cp(int fd_src, int fd_dst)
{
	char buf;
	int bytes_read;
	while((bytes_read = read(fd_src, &buf, 1)) != 0)
    	{
        	write(fd_dst, &buf, bytes_read);
	}

}

int main(int argc, char* argv[])
{
	int fd1, fd2;
	
	if(argc <  3)
	{
		fprintf(stderr, "Use %s <src_file> <dst_file>\n", argv[0]);
		fprintf(stderr, "stdin and stdout will be used\n");
		fd1 = 0;
		fd2 = 1;
	}
	else
	{
		fd1 = open(argv[1], O_RDONLY);
 		fd2 = open(argv[2], O_WRONLY | O_CREAT, 0666);
	}

	if ((fd1 < 0) || (fd2 < 0)) { fprintf(stderr, "Cant open files\n"); return -1;}
	else 
	{
		dup2(fd1, STDIN_FILENO);
		dup2(fd2, STDOUT_FILENO);
		cp(STDIN_FILENO, STDOUT_FILENO);
	}

  	return 0;
}

