#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

char message[] = "First string\r\nSecond string\r\nThird string\r\n";

int open_wrapper(char *filename, int flags, mode_t mode)
{
	int fd;
	if((fd = open(filename, flags, mode)) == -1)
	{
		perror("Error");
		fprintf(stderr, "Errno = %d, sys_errlist[%d] = %s\n",
					errno, errno, sys_errlist[errno]);
	}
	else printf("Open\n");
	return fd;
}

static char readbuf[256];
int main (int argc, char **argv)
{
	int fd, flags, n;
	off_t offset; 
	int whence;
	mode_t mode;
	
	if(argc < 4) 
	{
		fprintf(stderr, "Specify filename to open,  offset and whence for lseek\n");
		return -1;
	}
	flags = O_CREAT | O_RDWR | O_TRUNC;
	mode = 0666;
	
	if(sscanf(argv[2], "%d", &offset) != 1) return -1; 
	switch (argv[3][0]) {
		case 's': whence = SEEK_SET;
			break;
		case 'c': whence = SEEK_CUR;
			break;
		case 'e': whence = SEEK_END;
			break;
	}
	
	printf("Open file(rw + create)");
	fd = open_wrapper(argv[1], flags, mode);
	if(fd < 0) return -1;
	else printf("fd=%d\n", fd);
		
	printf("Try to do lseek\n");
	offset = 500;
	off_t offt = lseek(fd, offset, whence);
	printf("retval=%d\n", offt);
	if(offt < 0) return -1;

	printf("Try to write in file\n");
	n = write(fd, message, sizeof(message));
	printf("write retval=%d\n", n);
	printf("Close file\n");
	close(fd);
	
	return 0;
}

