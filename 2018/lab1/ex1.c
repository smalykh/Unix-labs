#include <stdio.h>
#include <errno.h>

int main (int argc, char **argv)
{
	int fd;
	if(argc < 2) 
	{
		fprintf(stderr, "Specify filename to open\n");
		return -1;
	}

	if((fd = open(argv[1], 0) == -1))
	{
		perror("Error");
		fprintf(stderr, "Errno = %d, sys_errlist[%d] = %s\n",
					errno, errno, sys_errlist[errno]);
	}
	else printf("Open\n");
	return 0;
}

