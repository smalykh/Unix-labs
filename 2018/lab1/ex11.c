#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


int open_wrapper(char *filename, int flags, mode_t mode)
{
	int fd;
	if((fd = open(filename, flags, mode)) == -1)
	{
		perror("Error");
	}
	else printf("Open\n");
	return fd;
}

static char readbuf[256];
int main (int argc, char **argv)
{
	int fd, flags, n;
	off_t offset; 
	int whence;
	mode_t mode;
    int i;	
	if(argc < 2) 
	{
		fprintf(stderr, "Specify filenames\n");
		return -1;
	}
	flags = O_RDONLY;
	mode = 0666;
	
	off_t offt, max_len = 0;
	for(i = 1; i < argc; i++)
	{
        fd = open_wrapper(argv[i], flags, mode);
	    if(fd < 0) return -1;
		offt = lseek(fd, 0, SEEK_END);
		if(offt > max_len) max_len = offt;
	}
	printf("Max_len=%d\n", max_len);
	close(fd);
	
	return 0;
}
