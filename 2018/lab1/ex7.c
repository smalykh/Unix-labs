#include <unistd.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
 
int main(int argc, char **argv)
{
	if(argc != 2) return 1;
 
	struct stat sf;
	if(stat(argv[1], &sf) < 0)    return 1;
 
	printf("Information for %s\n",argv[1]);
	printf("---------------------------\n");
 
	printf("ID:		%ld\n", 	(unsigned long) sf.st_dev);
	printf("I-node number:	%ld\n", 	(long) sf.st_ino);
	printf("Mode:		%lo (octal)\n", (unsigned long) sf.st_mode);
	printf("Link count:     %ld\n", 	(long) sf.st_nlink);
   printf("Ownership:      UID=%ld\n", 	(long) sf.st_uid);
   printf("Ownership:      GID=%ld\n", 	(long) sf.st_gid);
	printf("Device ID:	%ld\n", 	(unsigned long) sf.st_rdev);
	printf("Pref I/O bs:	%ld bytes\n", 	(long) sf.st_blksize);
	printf("File size:      %lld bytes\n",	(long long) sf.st_size);
	printf("Blocks alloc:   %lld\n",	(long long) sf.st_blocks);

	
	printf("\n\n");
	printf( (S_ISDIR(sf.st_mode))  ? "d" : "-");
	printf( (sf.st_mode & S_IRUSR) ? "r" : "-");
	printf( (sf.st_mode & S_IWUSR) ? "w" : "-");
	printf( (sf.st_mode & S_IXUSR) ? "x" : "-");
	printf( (sf.st_mode & S_IRGRP) ? "r" : "-");
	printf( (sf.st_mode & S_IWGRP) ? "w" : "-");
	printf( (sf.st_mode & S_IXGRP) ? "x" : "-");
	printf( (sf.st_mode & S_IROTH) ? "r" : "-");
	printf( (sf.st_mode & S_IWOTH) ? "w" : "-");
	printf( (sf.st_mode & S_IXOTH) ? "x" : "-");
	printf("\n\n");
 
	printf("File type:                ");
	switch (sf.st_mode & S_IFMT) {
           case S_IFBLK:  printf("block device\n");            break;
           case S_IFCHR:  printf("character device\n");        break;
           case S_IFDIR:  printf("directory\n");               break;
           case S_IFIFO:  printf("FIFO/pipe\n");               break;
           case S_IFLNK:  printf("symlink\n");                 break;
           case S_IFREG:  printf("regular file\n");            break;
           case S_IFSOCK: printf("socket\n");                  break;
           default:       printf("unknown?\n");                break;
	}	

 
    return 0;
}
