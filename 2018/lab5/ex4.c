#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include <sys/msg.h>

#define MY_DATA 3
struct my_msgbuf {
    long mtype;
    char payload[MY_DATA]; 
};

int read_message(int qid, long type, struct my_msgbuf *qbuf)
{
    int res;
    res = msgrcv(qid, qbuf, MY_DATA -1, type, MSG_NOERROR);
    if (res == -1) {
        perror("Msgrcv: ");
    }
    printf("ret=%d\n", res);
    return res;
}

int main (int argc, char* argv[]) {
    int msqid = atoi(argv[1]);
    int mtype = atoi(argv[2]);
    int ret;

    struct my_msgbuf msg1 = {0};
    ret = read_message(msqid, mtype, &msg1);
    if(ret > 0) 
	printf("Message 1: %s\n", msg1.payload);
    
    struct my_msgbuf msg2 = {0};
    ret = read_message(msqid, mtype, &msg2);
    if(ret > 0) 
	printf("Message 2: %s\n", msg2.payload);
    
    struct my_msgbuf msg3 = {0};
    ret = read_message(msqid, mtype, &msg3);
    if(ret > 0) 
	printf("Message 3: %s\n", msg3.payload);
}
