#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include <sys/msg.h>
#define MAX_L 		(256)

struct my_msgbuf {
    long mtype;
    int  msg_qid;
    char msg[MAX_L]; 
};

int send_message(int qid, struct my_msgbuf *qbuf)
{
    int res;
    int length;
    printf("mtype=%d\nid=%d\nmsg=%s\n", qbuf->mtype, qbuf->msg_qid, qbuf->msg);
    length = sizeof(struct my_msgbuf) - sizeof(long);        
    res = msgsnd(qid, qbuf, length, 0);
    if (res == -1) {
        perror("Msgsnd: ");
        return 1;
    }
    return 0;
}

int read_message(int qid, long type, struct my_msgbuf *qbuf)
{
    int res;
    int length;
    length = sizeof(struct my_msgbuf) - sizeof(long);   
    res = msgrcv(qid, qbuf, length, type, 0);
    if (res == -1) {
        perror("Msgrcv: ");
        return 1;
    }
    return 0;
}

int main (int argc, char* argv[]) {
    int key = ftok("./ex6_s", 'I');
    if (key == -1) {
        perror("Ftok: ");
        return 1;
    }
    int server_qid = msgget(key, 0666);
    if (server_qid == -1) {
        perror("Msgget: ");
        return 1;
    }

    key = ftok(argv[0], '1');
    if (key == -1) {
        perror("Ftok: ");
        return 1;
    }
    int client_qid = msgget(key, 0666 | IPC_CREAT);
    if (client_qid == -1) {
        perror("Msgget: ");
        return 1;
    }

    printf("Client has started\n");
    while (1) 
    {
    	struct my_msgbuf r_msg;
    	struct my_msgbuf s_msg;
	s_msg.mtype = 1;
        s_msg.msg_qid = client_qid;
    	printf("Enter your message:\n\t");
        scanf("%s", s_msg.msg);
        
	send_message(server_qid, &s_msg);
        read_message(client_qid, 1, &r_msg);
        printf("Response from server: %s\n", r_msg.msg);
    }
}
