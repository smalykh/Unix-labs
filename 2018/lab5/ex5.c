#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>

int main (int argc, char* argv[]) {

    if(argc < 2) { fprintf(stderr, "Specify id!\n"); return 1;}
    int msgid = atoi(argv[1]);

    if (msgctl(msgid, IPC_RMID, NULL) == -1) {
        fprintf(stderr, "Message queue could not be deleted.\n");
	perror("");
        exit(EXIT_FAILURE);
    }

    printf("Message queue was deleted.\n");

    return 0;
}
