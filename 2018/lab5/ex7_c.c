#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include <sys/msg.h>
#define MAX_L 		(256)

struct my_msgbuf {
    long mtype;
    int  msg_qid;
    char msg[MAX_L]; 
};

int send_message(int qid, struct my_msgbuf *qbuf)
{
    int res;
    int length;
    length = sizeof(struct my_msgbuf) - sizeof(long);        
    res = msgsnd(qid, qbuf, length, 0);
    if (res == -1) {
        perror("Msgsnd: ");
        return 1;
    }
    return 0;
}

int read_message(int qid, long type, struct my_msgbuf *qbuf)
{
    int res;
    int length;
    length = sizeof(struct my_msgbuf) - sizeof(long);   
    res = msgrcv(qid, qbuf, length, type, 0);
    if (res == -1) {
        perror("Msgrcv: ");
        return 1;
    }
    return 0;
}

int main (int argc, char* argv[]) {

    if(argc < 2) fprintf(stderr, "Specify ID!\n");
    int id = atoi(argv[1]);

    int key = ftok("./ex7_s", 'I' );
    if (key == -1) {
        perror("Ftok1: ");
        return 1;
    }
    int server_qid = msgget(key, 0666);
    if (server_qid == -1) {
        perror("Msgget1: ");
        return 1;
    }

    key = ftok(argv[0], id + '0');
    if (key == -1) {
        perror("Ftok2: ");
        return 1;
    }
    int client_qid = msgget(key, 0666 | IPC_CREAT);
    if (client_qid == -1) {
        perror("Msgget2:: ");
        return 1;
    }

    printf("Client has started\n");
    while (1) 
    {
    	struct my_msgbuf r_msg;
    	struct my_msgbuf s_msg;
	s_msg.mtype = 1;
    	printf("Enter destination ID (0 to read msgs):\n\t");
        char dstid = 0; scanf("%c", &dstid);
	if(dstid == '0')
	{
		read_message(client_qid, 1, &r_msg);
      		printf("Got message: %s\n", r_msg.msg);
	}
	else
	{
		int dstkey = ftok(argv[0], dstid);
    		if (dstkey == -1) {
       			perror("Ftok: ");
        		continue;
   		}
   		int dstqid = msgget(dstkey, 0666);
    		if (dstqid == -1) {
        		perror("Msgget: ");
        		continue;
    		}
	        s_msg.msg_qid = dstqid;
   	 	printf("Enter your message:\n\t");
       		scanf("%s", s_msg.msg);
        
		send_message(server_qid, &s_msg);
	}
    }
}
