#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <string.h>
#include <errno.h>
#include <sys/msg.h>

#define MY_DATA 32
struct my_msgbuf {
    long mtype;
    char payload[MY_DATA]; 
};

int read_message(int qid, long type, struct my_msgbuf *qbuf)
{
    int res;
    int length;
    length = sizeof(struct my_msgbuf) - sizeof(long);   
    res = msgrcv(qid, qbuf, length, type, IPC_NOWAIT);
    if (res == -1) {
        perror("Msgrcv: ");
    } 
    printf("ret=%d\n", res);
    return res;
}

int main (int argc, char* argv[]) {
    int msqid = atoi(argv[1]);
    int mtype = atoi(argv[2]);
    int ret;

    struct my_msgbuf msg1;
    ret = read_message(msqid, mtype, &msg1);
    if(ret > 0) 
	printf("Message 1: %s\n", msg1.payload);
    
    struct my_msgbuf msg2;
    ret = read_message(msqid, mtype, &msg2);
    if(ret > 0) 
	printf("Message 2: %s\n", msg2.payload);
    
    struct my_msgbuf msg3;
    ret = read_message(msqid, mtype, &msg3);
    if(ret > 0) 
	printf("Message 3: %s\n", msg3.payload);
}
