#include <stdio.h>
#include <signal.h>
static void sig_handler(int signo)
{
    int status;
    struct sigaction new_action;
    new_action.sa_handler = sig_handler;
    
    sigaction(SIGCHLD, &new_action, NULL);
    wait(&status);
    printf("New signal received!\n");
}

int main()
{
    int i;
    struct sigaction new_action;
    new_action.sa_handler = sig_handler;   
    sigaction(SIGCHLD, &new_action, NULL);

    if(fork() == 0) exit(0);
   
   //Infinity loop
    while (1) 
        pause();
}
