#include <stdio.h>
#include <signal.h>

static int counter;
static void sig_handler(int signo)
{
    struct sigaction new_action;
    new_action.sa_handler = sig_handler;
    sigaction(SIGUSR1, &new_action, NULL);
    
    printf("PID %d: %d\n", getpid(), counter);
    counter+=2;
}

int main()
{
    sigset_t mask, oldmask;
    struct  sigaction new_action;
    pid_t   pid;
    
    sigdelset(&mask, SIGUSR1);
    new_action.sa_handler = sig_handler;
    sigaction(SIGUSR1, &new_action, NULL);
    
    sigfillset(&mask); 
    sigprocmask(SIG_BLOCK, &mask, &oldmask);
     
    switch(pid = fork()) {
        case -1:
                exit(1);
        case 0:
                counter = 0;
                while(1) {
                    sigsuspend(&oldmask);
                    kill(getppid(), SIGUSR1);
                }
        default:
                counter = 1;
                while (1) {
                    kill(pid, SIGUSR1);
                    sigsuspend(&oldmask);
                }
    }
}
