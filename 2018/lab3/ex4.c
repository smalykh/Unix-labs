#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int main()
{
    pid_t pid;
    int status;
    int i, j, k;

    switch(pid = fork())
    {
        case -1:
            fprintf(stderr, "Oops..\n");
            return 1;
        case 0:
                for(i = 0; i < 1000; i++)
                {
                    fprintf(stderr, "i=%3d\n", i);
                    for(j = 0; j < 1000; j++)
                    for(k = 0; k < 1000; k++);
                }
                exit(0);
        default:    
                printf("Wait while child process ends..\n");
                wait(&status);
			    printf("Done. st=0x%x pid=%d\n", status, pid);  
                break;
    }
}

