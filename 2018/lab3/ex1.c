#include <stdio.h>
#include <signal.h>

static void sig_handler(int signo)
{
    signal(SIGINT, SIG_DFL);
    printf("New signal!\n");
}

int main()
{
    signal(SIGINT, sig_handler);
    pause();
}
