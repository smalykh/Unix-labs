#include <stdio.h>
#include <signal.h>

static void sig_handler(int signo)
{
    struct sigaction new_action;
    new_action.sa_handler = SIG_DFL;
    
    sigaction(SIGINT, &new_action, NULL);
    printf("New signal!\n");
}

int main()
{
    struct sigaction new_action;
    new_action.sa_handler = sig_handler;
    
    sigaction(SIGINT, &new_action, NULL);
   
   //Infinity loop
    while (1) 
        pause();
}
