#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
static void sig_handler(int signo)
{
    signal(SIGINT, sig_handler);
    printf("New signal!\n");
}

int main()
{
    pid_t pid;
    int status;
    int i, j, k;

    switch(pid = fork())
    {
        case -1:
            fprintf(stderr, "Oops..\n");
            return 1;
        case 0:
                signal(SIGUSR1, sig_handler);
                for(i = 0; i < 1000; i++)
                {
                    pause();
                    fprintf(stderr, "i=%3d\n", i);
                }
                return 0;
        default: 
                printf("Send signal to child..\n");
                sleep(3);
                kill(pid, SIGUSR1);
                printf("Wait while child process ends..\n");
                wait(&status);
			    printf("Done. st=0x%x pid=%d\n", status, pid);  
                break;
    }
}

