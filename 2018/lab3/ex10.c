#include <stdio.h>
#include <signal.h>

static void sig_handler(int signo)
{
    struct sigaction new_action;
    
    new_action.sa_handler = sig_handler;
    sigfillset(&new_action.sa_mask); 
   // sigdelset(new_action.sa_mask, SIGINT);
    sigprocmask(SIG_BLOCK, &new_action.sa_mask, NULL);
    sigaction(SIGCHLD, &new_action, NULL);

    printf("Start very important code..\n");
    sleep(10);
    printf("End very important code..\n");
    
    sigprocmask(SIG_UNBLOCK, &new_action.sa_mask, NULL);
}

int main()
{
    struct sigaction new_action;
    new_action.sa_handler = sig_handler;
    sigaction(SIGCHLD, &new_action, NULL);
   
   //Infinity loop
    while (1) 
        pause();
}
