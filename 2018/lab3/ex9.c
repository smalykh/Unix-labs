#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

//#define USE_TRAP

static void sig_handler(int signo)
{
    fprintf(stderr, "Signal handled\n");
    signal(SIGALRM, sig_handler);
}

int main()
{
    pid_t pid;
    int status;
    int i, j, k;

    switch(pid = fork())
    {
        case -1:
            fprintf(stderr, "Oops..\n");
            return 1;
        case 0:
                #ifdef USE_TRAP
                signal(SIGALRM, sig_handler); 
                #endif
                for(i = 0; i < 1000; i++)
                {
                    alarm(2);
                    pause();
                    fprintf(stderr, "i=%3d\n", i);
                }
                return 0;
        default: 
                printf("Wait while child process ends..\n");
                wait(&status);
			    printf("Done. st=0x%x pid=%d\n", status, pid);  
                break;
    }
}

